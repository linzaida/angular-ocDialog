angular-ocDialog
=========================
基于Angular的窗体应用框架
---------------------------

## 项目状态
### 综合情况
综合案例，实际正在开发的项目
Demo: http://probedemo.or-change.cn/probe
托管地址: https://git.oschina.net/increase/LemonProbe.git

* Version（版本）: 0.4.2
* Author（作者）: Chao Li, Weilin Shi, Yueyu Li
* License（许可）: MIT
* Supported Browsers（支持的浏览器）: Chrome,Firefox,Safari,Opera,IE 9+,iOS Safari 7.1+,Android Browser 2.3+
* Document（文档）: 有标题的功能已经实现，但文档仍在努力完善

### submodules（子模块）:
* ocCore - 图形操作动作库: http://git.oschina.net/increase/angular-ocCore


## 快速开始
通过快速开始，您将了解到使用本框架的基本流程。
如果需要看看效果，可以访问test/debugger.html文件，里面含有一些不完整的例子。
关于更加综合的应用案例，欢迎大家了解我们团队关于WEB自动化测试的项目Lemonce - LemonProbe。
相关地址见[综合情况](#综合情况)。

另外，Lemonce v0.6.4 Alpha 1版本目前停止更新，Alpha 2正在紧张研发，感兴趣的朋友仍然可以通过访问：
(http://www.lemonce.net/) 来了解我们Alpha 1的产品

### 安装
引用脚本资源以及样式资源，注意需要加载ocCore的资源，项目地址见 ``项目状态 > 子模块``
```html
<script src="./ocCore_Path/dist/ocCore[.min].js"></script>
<script src="./ocDialog_Path/dist/ocDialog[.min].js"></script>
<link rel="stylesheet" src="./ocDialog_Path/dist/style.css" />
```

### 配置

```javascript
angular.module("YourApp", ['oc-dialog'])
    .config(function (ocDialogProvider) {
        ...
    
		// 配置窗体资源的路由
        ocDialogProvider.config("ROUTE_PATH", "./src/dialog/");
		// 窗体的基准z-index值
    	ocDialogProvider.config("BASE_Z_INDEX", 500);
        
        ...
    });
```

## 指令 - Directive
### ocDialogOpen - 根据配置打开窗口
### ocDialogSizeMode - 切换窗体最大化和普通
### ocDialogClose - 关闭窗口

### ocAlertOpen - 打开警告窗口

### ocFileOpen - 打开文件列表窗口

## 服务 - Service
### ocDialog.queryDI(element) - 根据一个HTMLElement查询其所在的Dialog实例
### ocDialog.getNewDI(dialogFactoryName) - 根据Dialog工厂名获取一个Dialog实例
### ocDialog.alert(options) - 生成一个alertDialog实例并打开
### ocDialog.file(options) - 生成一个fileDialog实例并打开

## 设计窗体
使用ocDialog可以自定义一个指令
### ocDialog
定义一个最简单的没有内容窗体：
```html
<oc-dialog></oc-dialog>
```

### ocDialogTitle - 窗体标题
这个属性会影响到窗体的标题栏内容，值作为直接量读取：
```html
<oc-dialog oc-dialog-title="The title of myDialog"></oc-dialog>
```

### ocDialogIcon - 窗体标志Class
这个属性会影响到窗体左上角的图标区的class，配合自定义的css样式可以设定这个区域的样式：
```html
<oc-dialog oc-dialog-icon="lemoncelogo"></oc-dialog>
```

### ocDialogSwitch - 窗体开关配置
这个属性用于隐藏右上角的按钮，M隐藏最小化按钮，S隐藏尺寸模式按钮：
```html
<oc-dialog oc-dialog-switch="MS"></oc-dialog>
```

### 编码过程
根据[配置](#配置)的情况，在项目路径下 ``src/dialog/`` 目录内创建窗口定义``welcome.html``。
创建定义一个简单的登陆窗口，窗体的布局方式参见[emGrid](#emGrid)：
```html
<oc-dialog  style="width:10.2rem;height:6rem" oc-dialog-title="登陆窗口" oc-dialog-switch="MS">
	<div class="grid" ng-controller="Login">
		<label style="top:0rem;left:0.3rem;">输入令牌：</label>
		<input type="text" style="top:1.3rem;left:0.3rem;width:16em;">
		<button oc-dialog-open="main"
			style="right:0.3rem;bottom:0.3rem;height:1.4rem;width:4.3rem">取消</button>
	</div>
</oc-dialog>
```
在html中编写一个HTML元素来打开该窗体，引入的脚本资源除了Angular以外还包括[安装](#安装)中所提到的。
应用的引导文件典型的如下：
```html
<!DOCTYPE html>
<html ng-app="probe" ng-controller="Global" style="font-size: 20px;">
	<head>
		<meta charset="utf-8" />
		<script src="[yourPath]/angular.min.js"></script>
		<!-- 《安装》一节所需要引用的资源 -->
	</head>
	<body>
		<a oc-dialog-open="welcomeDialog">打开欢迎登陆窗口</a>
	</body>
</html>
```
使用了名为``Global``的控制器，主要提供打开welcome窗口的配置：
```javascript
youApp.controller('Global', function ($scope) {
	$scope.welcomeDialog = {
		name: 'welcome'
	};
});
```
运行应用，点击``打开欢迎登陆窗口``按钮，打开了之前定义的窗口。
除此之外，可以继续点击该按钮无限打开welcome窗体工厂创建的窗体实例。

### emGrid
支持全局缩放的2D窗体布局方法，该特性用于支持rem单位的浏览器上

## 元件
模仿常用的操作系统窗体编程的控件抽象的控件集合
### 工具栏

### 操作柄

### 按钮 / 输入框
位于Dialog内部的&lt;button>, &lt;input>都会受到影响

### ocAddress - 地址栏
操作页面中某个iframe的控件

### ocListMenu - 列表菜单
仿Windows 10 经典模式的列表菜单

### ocTabs - 选项卡
仿Windows 8/10 样式的列表菜单

### ocGrid - 矩阵表格
类似Excel的二维数组处理控件

### ocDataList - 数据列表
仿Windows文件列表的控件