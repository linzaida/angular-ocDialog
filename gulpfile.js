var gulp = require('gulp');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var include = require('gulp-include');
var ngMin = require('gulp-ng-annotate');

var less = require('gulp-less');
var cleanCSS =require('less-plugin-clean-css'),
	minify = new cleanCSS({advanced: true});

gulp.task('default', function(){
	return gulp.src('./src/adaptor.js')
		.pipe(include())
		.pipe(rename('ocDialog.js'))
		.pipe(gulp.dest('./dist'))
		.pipe(rename('ocDialog.min.js'))
		.pipe(ngMin())
		.pipe(uglify())
		.on("error", console.log)
		.pipe(gulp.dest('./dist'));
});

gulp.task("less", function(){
	return gulp.src('./resources/less/style.less')
		.pipe(less({
			plugins: [minify]
		}))
		.pipe(gulp.dest('./dist'));
});
