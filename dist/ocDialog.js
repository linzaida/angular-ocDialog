/*!
 * Angularjs - ocDialog Module v0.2.1
 *
 * http://or-change.cn
 * Copyright 2015 OrChange Inc. All rights reserved.
 * Licensed under the MIT License.
 */

(function (root, factory) {
	'use strict';
    if (typeof module !== 'undefined' && module.exports) {
        // CommonJS
        module.exports = factory(require('angular'));
    } else if (typeof define === 'function' && define.amd) {
        // AMD
        define(['angular'], factory);
    } else {
        // Global Variables
        factory(root.angular);
    }
}(this, function (angular) {
	'use strict';
	// Compile here	/*jslint vars: true, forin: true, plusplus: true */
	/*global angular: false, ocDialog: false */
	
	var debug = {
		"#2301": "Undefined dialogName to open.",
		"#1301": "Dialog has been active. So it could not be actived again.",
		"#1302": "Dialog has not been active. So it could not be deactived again.",
		"#1101": "The config item is not existed."
	};
	
	// Default configure of ocDialog service. And private.
	var defaults = {
		"BASE_Z_INDEX": 200,
		"ROUTE_PATH": './dialog',
		"ROUTE_SUFFIX": '.html',
		"ROUTE_PREFIX": ''
	};
	
	//var $body = angular.element(document.body);
	var ocDialog = angular.module('oc-dialog', ['ocCore']);
	
	/*jslint vars: true, forin: true, nomen: true, sloppy: true */
	/*global angular: false, DialogInstance, DialogManager */
	var DialogFactory = function DialogFactory(dialogFactoryName, dialogRaw) {
		this.$name = dialogFactoryName;
		this.$dialogRaw = dialogRaw;
	
		DialogManager.factoryList[dialogFactoryName] = this;
	}, $DF = DialogFactory.prototype;
	// $DF.getDialogElement defined in service/ocDialog.js
	
	/*jslint vars: true, forin: true, nomen: true, sloppy: true */
	/*global $body, DialogManager, angular */
	
	var DialogInstance = function (element, scope) {
		this.$$scope = scope;
	
		this.$$child = null;
		this.$$parent = null;
		this.$$element = element;
	
		element.data("dialogInstance", this)
			.on('mousedown', this.$activeFn);
	
	}, $DI = DialogInstance.prototype;
	
	$DI.$activeFn = function (event) {
		angular.element(this).data('dialogInstance').activeDialog();
	};
	
	$DI.$placeDialogTo = function (position) {
		var wWidth = window.innerWidth,
			wHeight = window.innerHeight,
			dWidth = this.$$element[0].offsetWidth,
			dHeight = this.$$element[0].offsetHeight;
	
		if (!position || position === 'center') {
			this.$$scope.$originPosition = {
				top: Math.max((wHeight - dHeight) / 2, 0) + 'px',
				left: Math.max((wWidth - dWidth) / 2, 0) + 'px'
			};
			this.$$element.css(this.$$scope.$originPosition);
		}
	
		return this;
	};
	
	$DI.$scope = function (callback) {
		var scope = this.$$scope, that = this;
		callback.call(this, scope);
	
		return this;
	};
	
	$DI.getElement = function () {
		return this.$$element;
	};
	
	$DI.setBlock = function (aim) {
		this.$$element[aim ? 'addClass' : 'removeClass']('block');
		this.$$element[aim ? 'unbind' : 'bind']('mousedown', this.$activeFn);
	
		return this;
	};
	
	$DI.setChildDialog = function (childDialog) {
		this.$$child = childDialog || null;
	
		return this;
	};
	
	$DI.setParentDialog = function (parentDialog) {
		this.$$parent = parentDialog || null;
	
		return this;
	};
	
	$DI.activeDialog = function () {
		DialogManager.activeDialogInstance(this).refreshZIndex();
	
		return this;
	};
	
	$DI.openDialogFrom = function (parent) {
		angular.element(document.body).append(this.$$element);
		DialogManager.registerDialogInstance(this).refreshZIndex();
	
		this.$placeDialogTo().switchDialogSizeModeTo(this.$$scope.isFull);
	
		if (parent) {
			parent.setChildDialog(this).setBlock(true);
			this.setParentDialog(parent);
		}
	
		return this;
	};
	
	$DI.closeDialog = function () {
		// [C]losed [C[all[b]ack
		var CCb = this.$$scope.closedCallback;
	
		this.$$element.remove();
		DialogManager.destroyDialogInstance(this);
	
	
		if (this.$$parent !== null) {
			this.$$parent.$$scope.$apply(function () {
				(CCb || angular.noop)();
			});
			this.$$parent.setBlock(false).setChildDialog();
		}
	
		return this;
	};
	
	$DI.switchDialogSizeModeTo = function (isFull) {
		var scope = this.$$scope,
			$ = this.$$element,
			DraI = this.$$element.data('draggableInstance');
	
		if (isFull) {
			DraI.unlink();
			scope.$originPosition = {
				top: $.css('top'),
				left: $.css('left')
			};
			$.css({
				top: 0,
				left: 0
			});
		} else {
			DraI.relink();
			$.css({
				top: scope.$originPosition.top,
				left: scope.$originPosition.left
			});
		}
	
		return this;
	};
	
	$DI.minimizeDialog = function () {
		//TODO
	};
	
	/*jslint vars: true, forin: true, sloppy: true */
	/*global angular: false, ocDialog: false, debug, defaults */
	var DialogManager = {
		HEAD: null, // There must be only one actived DI at the same time.
		activedDialogStack: [], // storage Dialog Instance by opening order.
		factoryList: {} // storage dialog factory by name.
	}, DS = DialogManager.activedDialogStack, DM = DialogManager;
	
	DM.refreshZIndex = function () {
		var zIndex = defaults.BASE_Z_INDEX;
	
		angular.forEach(this.activedDialogStack, function (DI, index) {
			DI.getElement().css('z-index', zIndex + index);
		});
	};
	
	DM.queryDialogByElement = function (element) {
		return element.inheritedData("dialogInstance");
	};
	DM.getParent = DM.queryDialogByElement;
	
	DM.destroyDialogInstance = function (DI) {
		DS.splice(DS.indexOf(DI), 1);
		this.HEAD = DS[DS.length - 1];
	
		return this;
	};
	
	DM.registerDialogInstance = function (DI) {
		this.activedDialogStack.push(DI);
		this.HEAD = DI;
	
		return this;
	};
	
	DM.activeDialogInstance = function (DI) {
		// !Notice: Array.prototype.splice will return a array.
		DS.push(DS.splice(DS.indexOf(DI), 1)[0]);
		this.HEAD = DI;
	
		return this;
	};
	
	/*jslint vars: true, forin: true, sloppy: true */
	/*global angular, DialogFactory, debug, DM, defaults, ocDialog, $DF */
	ocDialog.provider('ocDialog', function () {
		// Getter/Setter for defaults.
		var config = this.config = function (item, value) {
			if (!defaults.hasOwnProperty(item)) {
				throw new Error(debug["#1101"]);
			}
	
			if (angular.isDefined(value)) {
				defaults[item] = value;
			}
			return defaults[item];
		};
	
		this.$get = ['$q', '$http', '$rootScope', '$compile', function ($q, $http, $rootScope, $compile) {
	
			$DF.getDialogInstance = function () {
				// It makes the application layout can not compile "oc-dialog"
				// directive directly by this way.
				return $compile(this.$dialogRaw)($rootScope.$new()).data('dialogInstance');
			};
	
			var exports = {
				getDialogInstanceByDialogFactoryName: function (DFName) {
					var deffered = $q.defer();
	
					// DF (D)ialog (F)actory.
					if (DM.factoryList[DFName]) {
						deffered.resolve(DM.factoryList[DFName].getDialogInstance());
					} else {
						var url = defaults.ROUTE_PATH + defaults.ROUTE_PREFIX +
							DFName + defaults.ROUTE_SUFFIX;
	
						$http.get(url).success(function (raw) {
							deffered.resolve((new DialogFactory(DFName, raw)).getDialogInstance());
						}).error(function () {
							deffered.reject('Can NOT found by url: ' + url + '.');
						});
					}
	
					// Give a Dialog Element to callback function.
					return deffered.promise;
				},
				queryDialogByElement: DM.queryDialogByElement,
				alert: function (setup, parentDI) {
					this.getNewDI('oc-alert').then(function (DI) {
						angular.extend(DI.$$scope, setup);
						DI.openDialogFrom(parentDI);
					});
				},
				file: function (setup, parentDI) {
					this.getNewDI('oc-file').then(function (DI) {
						angular.extend(DI.$$scope, setup);
						DI.openDialogFrom(parentDI);
					});
				}
			};
	
			// Alias.
			exports.getNewDI = exports.getDialogInstanceByDialogFactoryName;
			exports.queryDI = exports.queryDialogByElement;
	
			return exports;
		}];
	});
	
	/*jslint vars: true, forin: true, sloppy: true */
	/*global angular, ocDialog, DialogManager, DialogFactory */
	ocDialog.directive('ocAddress', function ($interval) {
		return {
			template:
				'<div class="oc-address">' +
					'<input ng-model="address" ng-focus="isManual=true" ' +
						'ng-disabled="isCross" ' +
						'ng-keydown="gotoUrl($event)" />' +
					'<button class="reset" ng-click="reset()" ng-show="isCross"></button>' +
					'<button class="goto" ng-click="gotoUrl($event)" ' +
						'ng-mousedown="isManual=false" ng-hide="isCross"></button>' +
					'<button class="refresh" ng-click="refresh()" ' +
						'ng-mousedown="isManual=false" ng-hide="isCross"></button></div>',
			restrict: 'E',
			replace: true,
			scope: {
				iframeId: '@ocAddressIframe'
			},
			link: function (scope, element, attr) {
				setTimeout(function () {
					scope.iframe = document.querySelector('iframe#' + scope.iframeId);
				}, 0);
	
				scope.isCross = false;
				scope.isManual = false;
	
				// Refresh address per 1s.
				var refreshAddress = function () {
					try {
						if (!scope.isManual) {
							scope.address = scope.iframe.contentWindow.location.href;
							scope.isCross = false;
						}
					} catch (errorCrossDomain) {
						scope.isCross = true;
						scope.address = '正在跨域';
					}
				};
				$interval(refreshAddress, 600);
				refreshAddress();
	
				// Jump page button.
				scope.gotoUrl = function (event) {
	
					if (event.which === 1 || event.which === 13) {
						event.target.blur();
						scope.iframe.src = scope.address;
						scope.isManual = false;
					}
				};
	
				// Refresh page button.
				scope.refresh = function () {
					scope.iframe.contentWindow.location.reload();
				};
	
				// Reset src to '/' when iframe in cross-domain.
				scope.reset = function () {
					scope.isCross = false;
					scope.iframe.src = '/';
				};
			}
		};
	});
	
	/*jslint vars: true, forin: true, sloppy: true */
	/*global DialogInstance: false, ocDialog: false */
	ocDialog.directive('ocAlert', function () {
		return {
			restrict: 'E',
			transclude: true,
			replace: true,
			template:
				'<div class="oc-alert" oc-draggable>' +
					'<header><label>{{title}}</label></header>' +
					'<nav><button class="danger close" oc-dialog-close></button></nav>' +
					'<section><p>{{message}}</p>' +
						'<button class="text-but confirm" ng-mousedown="callFn()" oc-dialog-close>确定</button>' +
						'<button class="text-but cancel" oc-dialog-close>取消</button>' +
					'</section></div>',
			scope: {},
			link: function (scope, element, attr) {
				return new DialogInstance(element, scope);
			}
		};
	});
	
	/*jslint vars: true, forin: true, sloppy: true */
	/*global angular, ocDialog, DialogManager, DialogFactory */
	ocDialog.directive("ocDataList", function () {
		return {
			restrict: 'E',
			replace: true,
			scope: {
				setup: '='
			},
			template:
				'<div class="oc-data-list" ng-click="clearSelect()"><header><label ng-repeat="l in field track by $index"' +
					'ng-style="{width:l.width||\'125px\'}"><hr/>{{l.title}}</label></header>' +
					'<section><table><thead><tr><th ng-repeat="l in field track by $index">' +
						'<div ng-style="{width:l.width||\'125px\'}">&nbsp;</div></th></tr></thead>' +
	
						'<tbody><tr ng-click="select($index, $event)" ' +
							'ng-class="{active:setup.selected==$index}" ' +
							'ng-repeat="r in data track by $index">' +
							'<td ng-repeat="l in field track by $index">' +
								'<span>{{r[l.key]}}</span></td>' +
					'</tbody></table></section></div>',
			link: function (scope, element, attrs) {
				scope.data = scope.setup.data;
				scope.field = scope.setup.field;
	
				// A function call when a item was selected by click.
				scope.select = function (index, event) {
					event.stopPropagation();
	
					if (scope.setup.selected === index) {
						(scope.setup.visitFn || angular.noop).call(scope.data, index, event);
					} else {
						scope.setup.selected = index;
						(scope.setup.selectFn || angular.noop).call(scope.data, index, event);
					}
				};
	
				scope.clearSelect = function () {
					scope.setup.selected = -1;
				};
			}
		};
	});
	
	/*jslint vars: true, forin: true, sloppy: true */
	/*global DialogInstance: false, ocDialog: false */
	ocDialog.directive('ocDialog', function () {
		return {
			restrict: 'E',
			transclude: true,
			replace: true,
			template:
				'<div class="oc-dialog" oc-draggable ng-class="{full: isFull}">' +
					'<header oc-draggable-handle><i ng-class="iconName"></i><hr/>' +
						'<button class="ico" ng-repeat="B in buttons" ng-class="B.className" ng-click="B.fn()"></button>' +
						'<hr ng-if="buttons" /><label>{{title}}</label>' +
					'</header>' +
					'<nav>' +
						'<button class="minimize" oc-dialog-minimize ng-hide="isShowMin"></button>' +
						'<button class="undo" oc-dialog-size-mode  ng-hide="isShowSize"></button>' +
						'<button class="danger close" oc-dialog-close></button>' +
					'</nav>' +
					'<section ng-transclude></section></div>',
			scope: {
				title: "@ocDialogTitle", // The title text of this dialog.
				isFull: "@ocDialogIsFull", // The dialog is full-screen or not.
				iconName: "@ocDialogIcon", // Icon class name in dialog.
				MS: "@ocDialogSwitch"
			},
			compile: function () {
				return {
					pre: function (scope, element, attr) {
						scope.MS = scope.MS || '';
						scope.isShowMin = scope.MS.indexOf('M') !== -1;
						scope.isShowSize = scope.MS.indexOf('S') !== -1;
	
						//TODO 如果最大化打开，解绑拖动
	
						return new DialogInstance(element, scope);
					}
				};
			}
		};
	});
	
	/*jslint vars: true, forin: true, sloppy: true */
	/*global DialogInstance, ocDialog, angular */
	ocDialog.directive('ocFile', function ($filter) {
		var dateFilter = $filter('date');
	
		return {
			restrict: 'E',
			transclude: true,
			replace: true,
			template:
				'<div class="oc-file" oc-draggable>' +
					'<header oc-draggable-handle><label>{{action}} - {{title}}</label></header>' +
					'<nav><button class="danger close" oc-dialog-close></button></nav>' +
					'<section><nav class="oc-toolbar" style="border-bottom:1px solid #ccc"><input /></nav>' +
						'<oc-data-list setup="file"></oc-data-list>' +
						'<input ng-model="fileName" />' +
						'<button class="text-but cancel" oc-dialog-close ' +
							'ng-click="closeWith($event)">{{action}}</button>' +
					'</section></div>',
			scope: {},
			compile: function () {
				return {
					pre: function (scope, element, attr) {
						var data = [];
						scope.keyName = scope.name || 'name';
						scope.keyUpdateTime = scope.updateTime || 'updateTime';
						scope.dataList = scope.dataList || [{name: '未配置列表数据', updateTime: Date.now()}];
	
						angular.forEach(scope.dataList, function (row, index) {
							this.push({
								name: row[scope.keyName],
								updateTime: dateFilter(row[scope.keyUpdateTime], 'yyyy-MM-dd')
							});
						}, data);
	
						scope.file = {
							field: [
								{
									key: 'name',
									title: '名称',
									width: '19rem'
								},
								{
									key: 'updateTime',
									title: '修改日期',
									width: '4rem'
								}
							],
							data: data,
							selectFn: function (index) {
								scope.fileName = this[index][scope.keyName];
							}
						};
	
						// Set the action button behiever when it was clicked.
						scope.closeWith = function (event) {
							(scope.actionFn || angular.noop)(scope.file.selected, scope.fileName, event);
						};
	
						// The DI of this oc-file dialog.
						return new DialogInstance(element, scope);
					}
				};
			}
		};
	});
	
	/*jslint vars: true, forin: true, sloppy: true */
	/*global angular, ocDialog, DialogManager, DialogFactory */
	var throttle = function throttle(callback, limit) {
		//from underscore
		var wait = false;
		return function () {
			if (!wait) {
				callback.apply(this, arguments);
				wait = true;
				setTimeout(function () {
					wait = false;
				}, limit);
			}
		};
	}, titleMapping = [
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
		'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
		'Y', 'Z'
	];
	
	ocDialog.directive("ocGrid", function () {
		return {
			restrict: 'E',
			replace: true,
			controller: function () {},
			scope: {
				data: '=ocGridData',
				titles: '=ocGridTitles'
			},
			template:
				'<div class="oc-grid"><section class="oc-grid-bisect">Excel</section>' +
	
					// 横向行表头
					'<section class="oc-grid-row-header" style="left:0;">' +
					'<div ng-repeat="cell in titles" style="width: {{thSize[$index]}}px;"' +
					'ng-class="{selected:pos.col==$index}" ' +
					'>{{cell}}<hr ng-mousedown="resize($event, $index)" /></div>' +
					'</section>' +
	
					// 纵向列表头
					'<section class="oc-grid-col-header" style="top:0;">' +
					'<div style="height: 30px" ng-repeat="row in data track by $index"' +
					'ng-class="{selected:pos.row==$index}" ' +
					'>{{$index}}</div>' +
					'</section>' +
	
					// 数据滚动区
					'<div class="oc-grid-inner"><table>' +
	
						// 数据行表头宽度控制
						'<thead><tr><th ng-repeat="cell in data[0] track by $index">' +
						'<div style="width: {{thSize[$index]}}px"></div></th></tr></thead>' +
	
						// 数据渲染区
						'<tbody><tr ng-repeat="rowData in data track by $index" style="height:30px;">' +
						'<td ng-repeat="cellData in rowData track by $index" ' +
						'ng-class="{selected:pos.row==$parent.$index && pos.col==$index}" ' +
						'ng-click="selectCell($parent.$index, $index, $event)" ng-dblclick="editCell($event)">' +
						'<div>{{data[$parent.$index][$index]}}</div></td></tr></tbody>' +
					'</table><textarea ng-model="data[pos.row][pos.col]" ng-show="isEditable" class="oc-grid-input"' +
					' style="left:{{input.left}}px;top:{{input.top}}px;width:{{input.width}}px;height:{{input.height}}px">' +
					'</textarea></div></div>',
			link: function (scope, element, attr) {
				// 整理数据
				scope.thSize = []; //行头宽度表
				scope.titles = scope.titles || [];
	
				angular.forEach(scope.data[0], function (cell, index) {
					scope.thSize[index] = 120; //初始化行头宽度
					scope.titles[index] = titleMapping[index];
				});
	
	
				// 调整宽度
				var initLeft, initWidth, resizeHandleIndex, minWidth = 10;
	
				var resizing = function (event) {
					scope.$apply(function () {
						scope.thSize[resizeHandleIndex] =
							Math.max(initWidth + event.clientX - initLeft, minWidth);
					});
				};
	
				var cancelResizable = function () {
					element.off("mousemove", resizing);
					element.off("mouseup", cancelResizable);
				};
	
				scope.resize = function (event, index) {
					scope.isEditable = false;
	
					initLeft = event.clientX;
					initWidth = scope.thSize[index];
					resizeHandleIndex = index;
	
					element.on("mousemove", resizing).on("mouseup", cancelResizable);
				};
	
				// 选择单元格
				scope.pos = {
					row: 1,
					col: 1
				};
				scope.selectCell = function (row, col, event) {
					scope.isEditable = false;
					scope.pos = {
						row: row,
						col: col
					};
				};
	
				// 编辑单元格
				scope.isEditable = false;
				scope.input = {
					top: 0,
					left: 0,
					width: 0,
					height: 0
				};
				scope.editCell = function (event) {
					scope.isEditable = true;
					var $ = event.target.offsetParent;
	
					scope.input = {
						top: $.offsetTop,
						left: $.offsetLeft,
						width: $.offsetWidth - 1,
						height: $.offsetHeight - 1
					};
	
					setTimeout(function () {
						scope.inputBox[0].focus();
					}, 0);
				};
			}
		};
	}).directive("ocGridInner", function () {
		return {
			restrict: "C",
			require: '^ocGrid',
			link: function (scope, element) {
				element.on('scroll', throttle(function () {
					scope.rowHeader[0].style.left = '-' + element[0].scrollLeft + 'px';
					scope.colHeader[0].style.top = '-' + element[0].scrollTop + 'px';
				}, 0));
			}
		};
	}).directive("ocGridRowHeader", function () {
		return {
			restrict: "C",
			require: '^ocGrid',
			link: function (scope, element) {
				scope.rowHeader = element;
			}
		};
	}).directive("ocGridInput", function () {
		return {
			restrict: "C",
			require: '^ocGrid',
			link: function (scope, element) {
				scope.inputBox = element;
			}
		};
	}).directive("ocGridColHeader", function () {
		return {
			restrict: "C",
			require: '^ocGrid',
			link: function (scope, element) {
				scope.colHeader = element;
			}
		};
	});
	
	
	/*jslint vars: true, forin: true, sloppy: true */
	/*global angular, ocDialog, DialogManager, evalEach */
	var evaluate = function (data) {
		if (data === null) {
			return "<hr />";
		}
		if (data.hasOwnProperty('attr')) {
			return '<li><a ' + data.attr + '>' + data.title + '</a></li>';
		}
		if (data.hasOwnProperty('sub')) {
			return '<li class="sub">' + data.title + '<ul>' + evalEach(data.sub) + '</ul></li>';
		}
	
		return '<li><a>' + data.title + '</a></li>';
	}, evalEach = function (raw) {
		return raw.reduce(function (out, item) {
			return out + evaluate(item);
		}, "");
	};
	
	ocDialog.directive("ocListMenu", function () {
		return {
			replace: true,
			controller: angular.noop,
			template:
				'<menu class="oc-menu" ng-class="{focus: isFocus}" ng-click="toggleMenu()">' +
					'<li ng-repeat="m in content track by $index" ng-click="">{{m.title}}' +
						'<ul oc-menu-sub="{{$index}}"></ul></li></menu>',
			restrict: 'E',
			scope: {
				content: "="
			},
			link: function (scope, element, attrs) {
				// Transfer the original scope into isolating scope.
				scope.originScope = scope.$parent;
	
				// Control oc-menu isolation state.
				scope.isFocus = false;
				scope.toggleMenu = function () {
					scope.isFocus = !scope.isFocus;
				};
			}
		};
	}).directive("ocMenuSub", function ($compile) {
		return {
			restrict: "A",
			require: '^ocListMenu',
			link: function (scope, element, attrs) {
				// Render the list in sub-menu.
				var subList = scope.content[attrs.ocMenuSub].menu;
				element.append($compile(evalEach(subList))(scope.originScope));
			}
		};
	});
	
	/*jslint vars: true, forin: true, sloppy: true */
	/*global angular, ocDialog, DialogManager, DialogFactory */
	ocDialog.directive('ocTabSet', function () {
		//TODO add disabled tab
		return {
			restrict: 'E',
			transclude: true,
			replace: true,
			scope: {
				setup: "=ocTabSetOpts"
			},
			controller: function ($scope) {
				console.log($scope);
				var ocTabs = $scope.ocTabs = [];
				$scope.select = function (tab) {
					angular.forEach(ocTabs, function (item) {
						item.selected = false;
					});
					tab.selected = true;
				};
	
				this.addTab = function (tab) {
					if (ocTabs.length === 0) {
						tab.selected = true;
					}
					ocTabs.push(tab);
				};
			},
			template:
				'<div class="oc-tabs">' +
					'<ul><li ng-repeat="tab in ocTabs" ng-click="select(tab)" title="{{tab.ocTabTitle}}" ' +
					'ng-class="{active:tab.selected}">{{tab.ocTabTitle}}</li></ul>' +
					'<div class="oc-tab-content" ng-transclude></div>' +
				'</div>'
		};
	}).directive('ocTab', function () {
		return {
			require: '^ocTabSet',
			restrict: 'E',
			transclude: true,
			replace: true,
			scope: {
				ocTabTitle: '@'
			},
			link: function (scope, element, attrs, ctrl) {
				ctrl.addTab(scope);
			},
			template: '<section ng-transclude ng-show="selected"></section>'
		};
	});
	
	/*jslint vars: true, forin: true, sloppy: true */
	/*global angular, ocDialog, DM, DialogFactory */
	ocDialog.directive('ocAlertOpen', function (ocDialog) {
		// Fixed template for a alert dialog.
		var DF = new DialogFactory('oc-alert', '<oc-alert></oc-alert>');
	
		return {
			restrict: 'A',
			scope: {
				setup: "=ocAlertOpen"
			},
			link: function (scope, element, attr) {
				element.on("click", function () {
					ocDialog.alert(angular.extend({
						title: '警告',
						message: '请确认该操作！',
						callFn: angular.noop
					}, scope.setup), DM.queryDialogByElement(element));
				});
			}
		};
	});
	
	/*jslint vars: true, forin: true, sloppy: true */
	/*global DialogManager: false, ocDialog: false */
	ocDialog.directive('ocDialogClose', function () {
		return {
			restrict: 'A',
			link: function (scope, element, attr) {
				element.on("click", function () {
					DialogManager.queryDialogByElement(element).closeDialog();
				});
			}
		};
	});
	
	/*jslint vars: true, forin: true, sloppy: true */
	/*global angular: false, ocDialog, DM */
	ocDialog.directive('ocDialogOpen', function (ocDialog) {
		// Try to get the dialog name if the directive DOM is in a Dialog.
		return {
			restrict: 'A',
			scope: {
				setup: "=ocDialogOpen"
			},
			link: function (scope, element, attr) {
				element.on("click", function () {
					ocDialog.getNewDI(scope.setup.name).then(function (DI) {
						angular.extend(DI.$$scope, scope.setup);
						DI.openDialogFrom(DM.queryDialogByElement(element));
					});
				});
			}
		};
	});
	
	/*jslint vars: true, forin: true, sloppy: true */
	/*global DialogManager: false, ocDialog: false */
	ocDialog.directive('ocDialogSizeMode', function () {
		return {
			restrict: 'A',
			link: function (scope, element, attr) {
				element.on("click", function () {
					var DI = DialogManager.queryDialogByElement(element);
	
					DI.$scope(function (scope) {
						scope.$apply(function () {
							scope.isFull = !scope.isFull;
							this.switchDialogSizeModeTo(scope.isFull);
						}.bind(this));
					});
				});
			}
		};
	});
	
	/*jslint vars: true, forin: true, sloppy: true */
	/*global angular, ocDialog, DM, DialogFactory */
	ocDialog.directive('ocFileOpen', function (ocDialog) {
		// Fixed template for a alert dialog.
		var DF = new DialogFactory('oc-file', '<oc-file></oc-file>');
		return {
			restrict: 'A',
			scope: {
				setup: "=ocFileOpen"
			},
			link: function (scope, element, attr) {
				element.on("click", function () {
					ocDialog.file(angular.extend({
						title: '未定义对象名称',
						action: '未知',
						callFn: angular.noop
					}, scope.setup), DM.queryDialogByElement(element));
				});
			}
		};
	});
	


}));
