/*jslint vars: true, forin: true, sloppy: true */
/*global angular, ocDialog, DialogManager, DialogFactory */
ocDialog.directive('ocTabSet', function () {
	//TODO add disabled tab
	return {
		restrict: 'E',
		transclude: true,
		replace: true,
		scope: {
			setup: "=ocTabSetOpts"
		},
		controller: function ($scope) {
			console.log($scope);
			var ocTabs = $scope.ocTabs = [];
			$scope.select = function (tab) {
				angular.forEach(ocTabs, function (item) {
					item.selected = false;
				});
				tab.selected = true;
			};

			this.addTab = function (tab) {
				if (ocTabs.length === 0) {
					tab.selected = true;
				}
				ocTabs.push(tab);
			};
		},
		template:
			'<div class="oc-tabs">' +
				'<ul><li ng-repeat="tab in ocTabs" ng-click="select(tab)" title="{{tab.ocTabTitle}}" ' +
				'ng-class="{active:tab.selected}">{{tab.ocTabTitle}}</li></ul>' +
				'<div class="oc-tab-content" ng-transclude></div>' +
			'</div>'
	};
}).directive('ocTab', function () {
	return {
		require: '^ocTabSet',
		restrict: 'E',
		transclude: true,
		replace: true,
		scope: {
			ocTabTitle: '@'
		},
		link: function (scope, element, attrs, ctrl) {
			ctrl.addTab(scope);
		},
		template: '<section ng-transclude ng-show="selected"></section>'
	};
});
