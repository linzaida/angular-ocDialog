/*jslint vars: true, forin: true, sloppy: true */
/*global DialogManager: false, ocDialog: false */
ocDialog.directive('ocDialogSizeMode', function () {
	return {
		restrict: 'A',
		link: function (scope, element, attr) {
			element.on("click", function () {
				var DI = DialogManager.queryDialogByElement(element);

				DI.$scope(function (scope) {
					scope.$apply(function () {
						scope.isFull = !scope.isFull;
						this.switchDialogSizeModeTo(scope.isFull);
					}.bind(this));
				});
			});
		}
	};
});
