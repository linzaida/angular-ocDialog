/*jslint vars: true, forin: true, sloppy: true */
/*global angular, ocDialog, DM, DialogFactory */
ocDialog.directive('ocFileOpen', function (ocDialog) {
	// Fixed template for a alert dialog.
	var DF = new DialogFactory('oc-file', '<oc-file></oc-file>');
	return {
		restrict: 'A',
		scope: {
			setup: "=ocFileOpen"
		},
		link: function (scope, element, attr) {
			element.on("click", function () {
				ocDialog.file(angular.extend({
					title: '未定义对象名称',
					action: '未知',
					callFn: angular.noop
				}, scope.setup), DM.queryDialogByElement(element));
			});
		}
	};
});
