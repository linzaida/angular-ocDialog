/*jslint vars: true, forin: true, sloppy: true */
/*global angular, ocDialog, DM, DialogFactory */
ocDialog.directive('ocAlertOpen', function (ocDialog) {
	// Fixed template for a alert dialog.
	var DF = new DialogFactory('oc-alert', '<oc-alert></oc-alert>');

	return {
		restrict: 'A',
		scope: {
			setup: "=ocAlertOpen"
		},
		link: function (scope, element, attr) {
			element.on("click", function () {
				ocDialog.alert(angular.extend({
					title: '警告',
					message: '请确认该操作！',
					callFn: angular.noop
				}, scope.setup), DM.queryDialogByElement(element));
			});
		}
	};
});
