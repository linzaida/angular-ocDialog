/*jslint vars: true, forin: true, sloppy: true */
/*global angular: false, ocDialog: false */
ocDialog.directive('ocDialogClose', function (ocDialog, $parse) {
	return {
		restrict: 'A',
		link: function (scope, element, attr) {
			element.on("click", function () {
				ocDialog.$getDialogByElement(element).minimizeDialog();
			});
		}
	};
});
