/*jslint vars: true, forin: true, sloppy: true */
/*global angular: false, ocDialog, DM */
ocDialog.directive('ocDialogOpen', function (ocDialog) {
	// Try to get the dialog name if the directive DOM is in a Dialog.
	return {
		restrict: 'A',
		scope: {
			setup: "=ocDialogOpen"
		},
		link: function (scope, element, attr) {
			element.on("click", function () {
				ocDialog.getNewDI(scope.setup.name).then(function (DI) {
					angular.extend(DI.$$scope, scope.setup);
					DI.openDialogFrom(DM.queryDialogByElement(element));
				});
			});
		}
	};
});
