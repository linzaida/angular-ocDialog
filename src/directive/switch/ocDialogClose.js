/*jslint vars: true, forin: true, sloppy: true */
/*global DialogManager: false, ocDialog: false */
ocDialog.directive('ocDialogClose', function () {
	return {
		restrict: 'A',
		link: function (scope, element, attr) {
			element.on("click", function () {
				DialogManager.queryDialogByElement(element).closeDialog();
			});
		}
	};
});
