/*jslint vars: true, forin: true, sloppy: true */
/*global DialogInstance: false, ocDialog: false */
ocDialog.directive('ocDialog', function () {
	return {
		restrict: 'E',
		transclude: true,
		replace: true,
		template:
			'<div class="oc-dialog" oc-draggable ng-class="{full: isFull}">' +
				'<header oc-draggable-handle><i ng-class="iconName"></i><hr/>' +
					'<button class="ico" ng-repeat="B in buttons" ng-class="B.className" ng-click="B.fn()"></button>' +
					'<hr ng-if="buttons" /><label>{{title}}</label>' +
				'</header>' +
				'<nav>' +
					'<button class="minimize" oc-dialog-minimize ng-hide="isShowMin"></button>' +
					'<button class="undo" oc-dialog-size-mode  ng-hide="isShowSize"></button>' +
					'<button class="danger close" oc-dialog-close></button>' +
				'</nav>' +
				'<section ng-transclude></section></div>',
		scope: {
			title: "@ocDialogTitle", // The title text of this dialog.
			isFull: "@ocDialogIsFull", // The dialog is full-screen or not.
			iconName: "@ocDialogIcon", // Icon class name in dialog.
			MS: "@ocDialogSwitch"
		},
		compile: function () {
			return {
				pre: function (scope, element, attr) {
					scope.MS = scope.MS || '';
					scope.isShowMin = scope.MS.indexOf('M') !== -1;
					scope.isShowSize = scope.MS.indexOf('S') !== -1;

					//TODO 如果最大化打开，解绑拖动

					return new DialogInstance(element, scope);
				}
			};
		}
	};
});
