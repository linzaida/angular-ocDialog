/*jslint vars: true, forin: true, sloppy: true */
/*global DialogInstance: false, ocDialog: false */
ocDialog.directive('ocAlert', function () {
	return {
		restrict: 'E',
		transclude: true,
		replace: true,
		template:
			'<div class="oc-alert" oc-draggable>' +
				'<header><label>{{title}}</label></header>' +
				'<nav><button class="danger close" oc-dialog-close></button></nav>' +
				'<section><p>{{message}}</p>' +
					'<button class="text-but confirm" ng-mousedown="callFn()" oc-dialog-close>确定</button>' +
					'<button class="text-but cancel" oc-dialog-close>取消</button>' +
				'</section></div>',
		scope: {},
		link: function (scope, element, attr) {
			return new DialogInstance(element, scope);
		}
	};
});
