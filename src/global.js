/*jslint vars: true, forin: true, plusplus: true */
/*global angular: false, ocDialog: false */

var debug = {
	"#2301": "Undefined dialogName to open.",
	"#1301": "Dialog has been active. So it could not be actived again.",
	"#1302": "Dialog has not been active. So it could not be deactived again.",
	"#1101": "The config item is not existed."
};

// Default configure of ocDialog service. And private.
var defaults = {
	"BASE_Z_INDEX": 200,
	"ROUTE_PATH": './dialog',
	"ROUTE_SUFFIX": '.html',
	"ROUTE_PREFIX": ''
};

//var $body = angular.element(document.body);
var ocDialog = angular.module('oc-dialog', ['ocCore']);
