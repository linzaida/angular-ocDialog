/*jslint vars: true, forin: true, nomen: true, sloppy: true */
/*global $body, DialogManager, angular */

var DialogInstance = function (element, scope) {
	this.$$scope = scope;

	this.$$child = null;
	this.$$parent = null;
	this.$$element = element;

	element.data("dialogInstance", this)
		.on('mousedown', this.$activeFn);

}, $DI = DialogInstance.prototype;

$DI.$activeFn = function (event) {
	angular.element(this).data('dialogInstance').activeDialog();
};

$DI.$placeDialogTo = function (position) {
	var wWidth = window.innerWidth,
		wHeight = window.innerHeight,
		dWidth = this.$$element[0].offsetWidth,
		dHeight = this.$$element[0].offsetHeight;

	if (!position || position === 'center') {
		this.$$scope.$originPosition = {
			top: Math.max((wHeight - dHeight) / 2, 0) + 'px',
			left: Math.max((wWidth - dWidth) / 2, 0) + 'px'
		};
		this.$$element.css(this.$$scope.$originPosition);
	}

	return this;
};

$DI.$scope = function (callback) {
	var scope = this.$$scope, that = this;
	callback.call(this, scope);

	return this;
};

$DI.getElement = function () {
	return this.$$element;
};

$DI.setBlock = function (aim) {
	this.$$element[aim ? 'addClass' : 'removeClass']('block');
	this.$$element[aim ? 'unbind' : 'bind']('mousedown', this.$activeFn);

	return this;
};

$DI.setChildDialog = function (childDialog) {
	this.$$child = childDialog || null;

	return this;
};

$DI.setParentDialog = function (parentDialog) {
	this.$$parent = parentDialog || null;

	return this;
};

$DI.activeDialog = function () {
	DialogManager.activeDialogInstance(this).refreshZIndex();

	return this;
};

$DI.openDialogFrom = function (parent) {
	angular.element(document.body).append(this.$$element);
	DialogManager.registerDialogInstance(this).refreshZIndex();

	this.$placeDialogTo().switchDialogSizeModeTo(this.$$scope.isFull);

	if (parent) {
		parent.setChildDialog(this).setBlock(true);
		this.setParentDialog(parent);
	}

	return this;
};

$DI.closeDialog = function () {
	// [C]losed [C[all[b]ack
	var CCb = this.$$scope.closedCallback;

	this.$$element.remove();
	DialogManager.destroyDialogInstance(this);


	if (this.$$parent !== null) {
		this.$$parent.$$scope.$apply(function () {
			(CCb || angular.noop)();
		});
		this.$$parent.setBlock(false).setChildDialog();
	}

	return this;
};

$DI.switchDialogSizeModeTo = function (isFull) {
	var scope = this.$$scope,
		$ = this.$$element,
		DraI = this.$$element.data('draggableInstance');

	if (isFull) {
		DraI.unlink();
		scope.$originPosition = {
			top: $.css('top'),
			left: $.css('left')
		};
		$.css({
			top: 0,
			left: 0
		});
	} else {
		DraI.relink();
		$.css({
			top: scope.$originPosition.top,
			left: scope.$originPosition.left
		});
	}

	return this;
};

$DI.minimizeDialog = function () {
	//TODO
};
