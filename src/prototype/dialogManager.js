/*jslint vars: true, forin: true, sloppy: true */
/*global angular: false, ocDialog: false, debug, defaults */
var DialogManager = {
	HEAD: null, // There must be only one actived DI at the same time.
	activedDialogStack: [], // storage Dialog Instance by opening order.
	factoryList: {} // storage dialog factory by name.
}, DS = DialogManager.activedDialogStack, DM = DialogManager;

DM.refreshZIndex = function () {
	var zIndex = defaults.BASE_Z_INDEX;

	angular.forEach(this.activedDialogStack, function (DI, index) {
		DI.getElement().css('z-index', zIndex + index);
	});
};

DM.queryDialogByElement = function (element) {
	return element.inheritedData("dialogInstance");
};
DM.getParent = DM.queryDialogByElement;

DM.destroyDialogInstance = function (DI) {
	DS.splice(DS.indexOf(DI), 1);
	this.HEAD = DS[DS.length - 1];

	return this;
};

DM.registerDialogInstance = function (DI) {
	this.activedDialogStack.push(DI);
	this.HEAD = DI;

	return this;
};

DM.activeDialogInstance = function (DI) {
	// !Notice: Array.prototype.splice will return a array.
	DS.push(DS.splice(DS.indexOf(DI), 1)[0]);
	this.HEAD = DI;

	return this;
};
