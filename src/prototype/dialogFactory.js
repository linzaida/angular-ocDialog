/*jslint vars: true, forin: true, nomen: true, sloppy: true */
/*global angular: false, DialogInstance, DialogManager */
var DialogFactory = function DialogFactory(dialogFactoryName, dialogRaw) {
	this.$name = dialogFactoryName;
	this.$dialogRaw = dialogRaw;

	DialogManager.factoryList[dialogFactoryName] = this;
}, $DF = DialogFactory.prototype;
// $DF.getDialogElement defined in service/ocDialog.js
