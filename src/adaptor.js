/*!
 * Angularjs - ocDialog Module v0.2.1
 *
 * http://or-change.cn
 * Copyright 2015 OrChange Inc. All rights reserved.
 * Licensed under the MIT License.
 */

(function (root, factory) {
	'use strict';
    if (typeof module !== 'undefined' && module.exports) {
        // CommonJS
        module.exports = factory(require('angular'));
    } else if (typeof define === 'function' && define.amd) {
        // AMD
        define(['angular'], factory);
    } else {
        // Global Variables
        factory(root.angular);
    }
}(this, function (angular) {
	'use strict';
	// Compile here
	//= include ./global.js
	//= include ./prototype/*.js
	//= include ./service/*.js
	//= include ./directive/*.js
	//= include ./directive/switch/!(ocDialogMinimize).js

}));
